﻿using ArchitectureDotnet.Extensions;
using System;

namespace ArchitectureDotnet
{
    /// <summary>
    /// Класс для работы с квадратными уравнениями
    /// </summary>
    public class QuadraticEquation
    {
        /// <summary>
        /// Метод для решения квадратного уравнения вида ax^2+bx+c=0
        /// </summary>
        /// <param name="a">Коэффициент a</param>
        /// <param name="b">Коэффициент b</param>
        /// <param name="c">Свободный член c</param>
        /// <returns>Массив решений</returns>
        public static double[] Solve(double a, double b, double c)
        {
            if (a.AreTrueEquals(0, 1e-12))
            {
                throw new ArgumentException("Коэффициент a не может быть равен нулю");
            }

            var d = b * b - 4 * a * c;
            if (d < 0)
            {
                return Array.Empty<double>();
            }
            var sqrtOfD = Math.Sqrt(d);
            return new double[] { (-b - sqrtOfD) / 2, (-b + sqrtOfD) / 2 };
        }
    }
}
