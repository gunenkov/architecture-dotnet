﻿using ArchitectureDotnet.Extensions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ArchitectureDotnet.Tests
{
    [TestClass()]
    public class QuadraticEquationTests
    {
        [TestMethod()]
        public void SolveTestTwoSameSolutions()
        {
            var expected = new double[] { 1, 1 };
            var actual = QuadraticEquation.Solve(1, -2, 1);
            CompareExpectedWithActual(expected, actual);
        }

        [TestMethod()]
        public void SolveTestTwoDifferentSolutions()
        {
            var expected = new double[] { -1, 1 };
            var actual = QuadraticEquation.Solve(1, 0, -1);
            CompareExpectedWithActual(expected, actual);
        }

        [TestMethod()]
        public void SolveTestNoSolutions()
        {
            Assert.AreEqual(0, QuadraticEquation.Solve(1, 0, 1).Length);
        }

        [TestMethod()]
        public void SolveTestValidCoefficientA()
        {
            Assert.ThrowsException<ArgumentException>(() => QuadraticEquation.Solve(double.Epsilon, 2, 1));
        }

        private void CompareExpectedWithActual(double[] expected, double[] actual)
        {
            Assert.AreEqual(expected.Length, actual.Length);
            Assert.AreEqual(true, expected[0].AreTrueEquals(actual[0], 1e-12));
            Assert.AreEqual(true, expected[1].AreTrueEquals(actual[1], 1e-12));
        }
    }
}
